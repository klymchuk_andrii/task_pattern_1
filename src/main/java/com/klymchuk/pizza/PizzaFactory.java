package com.klymchuk.pizza;

import com.klymchuk.pizza.pizzaImp.*;

public class PizzaFactory {

    public Pizza createPizza(PizzaType type) {
        switch (type) {
            case CHEESE:
                return new CheesePizza();
            case VEGGIE:
                return new VeggiePizza();
            case CLAM:
                return new ClamPizza();
            case PEPPERONI:
                return new PepperoniPizza();
            default:
                throw new RuntimeException("Can`t found Pizza type to create.");
        }
    }
}
