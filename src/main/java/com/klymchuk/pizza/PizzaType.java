package com.klymchuk.pizza;

public enum PizzaType {
    CHEESE, VEGGIE, CLAM, PEPPERONI
}
