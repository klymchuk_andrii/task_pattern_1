package com.klymchuk.view;

import com.klymchuk.pizza.Pizza;
import com.klymchuk.pizza.PizzaFactory;
import com.klymchuk.pizza.PizzaType;

public class View {
    public static void main(String[] args) {
        Pizza pizza = new PizzaFactory().createPizza(PizzaType.CHEESE);
    }
}
